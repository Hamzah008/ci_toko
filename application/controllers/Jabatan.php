<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jabatan extends CI_controller {

	public function __construct()
	{		
		parent::__construct();
		//load model terkait
		$this->load->model("jabatan_model");
	}
	
	public function index()
	{
		$this->listJabatan();
	}
	
	public function listjabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$this->load->view('HomeJabatan', $data);
	}


	public function input()
	{
		$data['data_barang'] = $this->jabatan_model->tampilDataJabatan();
		
		if (!empty($_REQUEST)) {
			$m_jabatan = $this->jabatan_model;
			$m_jabatan->save();
			redirect("jabatan/index", "refresh"); 
		}
		
		
		$this->load->view('InputJabatan', $data);
	}
	
	   public function detail_jabatan($kode_jabatan)
	   {
			$data['detail_jabatan']	= $this->jabatan_model->detail($kode_jabatan);
			$this->load->view('detail_jabatan', $data);   
	   }

	
	}
