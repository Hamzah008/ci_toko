<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Karyawan extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Karyawan_model");
		$this->load->model("jabatan_model");
		
	}
	
	public function index()
	
	{
		$this->listKaryawan();
	}
	
	public function listKaryawan()
	
	{
		$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$this->load->view('home', $data);
	}
	
	
		public function input()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		
		if (!empty($_REQUEST)) {
			$m_karyawan = $this->Karyawan_model;
			$m_karyawan->save();
			redirect("Karyawan/index", "refresh"); 
		}
		
		
		$this->load->view('InputKaryawan', $data);
	}
	
	   public function detailkaryawan($nik)
	   {
			$data['detail_karyawan']	= $this->Karyawan_model->detail($nik);
			$this->load->view('detail_karyawan', $data);   
	   }
	   
}

