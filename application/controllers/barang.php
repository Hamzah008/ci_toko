<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Barang extends CI_controller {

	public function __construct()
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("barang_model");
	}
	
	public function index()
	
	{
		$this->listBarang();
	}
	
	public function listBarang()
	
	{
		$data['data_barang'] = $this->barang_model->tampilDataBarang();
		$this->load->view('HomeBarang', $data);
	}


	public function input()
	{
		$data['data_jenis_barang'] = $this->barang_model->tampilDataBarang();
		
		if (!empty($_REQUEST)) {
			$barang = $this->barang_model;
			$barang->save();
			redirect("barang/index", "refresh"); 
		}
		
		
		$this->load->view('InputBarang', $data);
	}
	
	   public function detail_barang($kode_barang)
	   {
			$data['detail_barang']	= $this->barang_model->detail($kode_barang);
			$this->load->view('detail_barang', $data);   
	   }









}
